### Tailscale (personal/on-prem)

Ansible configuration for accessing a Ubuntu server from a Ubuntu desktop via Tailscale.

## Prerequisites

* Ansible installed locally
* Ubuntu 20.04 server or VM
* (Ubuntu) client machine(s)

## Ansible configuration

Grab a one-time registration key from your Tailscale [admin portal](https://login.tailscale.com/admin/authkeys) and add it to your playbook with the output from the below, remembering to save your vault password in your favoured password manager!
```
ansible-vault encrypt_string --ask-vault-pass -n reg_key
```
Alternatively, you could copy the key in plaintext if you're testing or aren't phased by the risks.

Ensure that you modify your `/etc/ansible/hosts` file with your desired server's IP with something like the below. You should also make sure that the `local_subnet` var matches the subnet your server is on to avoid losing access.
```
[tailscale]
tailscale01		ansible_host=10.0.0.1

```

Create an `ansible` user on your server and allocate passwordless sudo permissions. To do this, add a new user (`sudo adduser ansible && sudo adduser ansible sudo`) and edit `/etc/sudoers` by running `sudo visudo` and add the following line at the bottom:
```
ansible	ALL=(ALL:ALL) NOPASSWD:ALL
```

Make sure to copy your ssh key into `/home/ansible/.ssh/authorized_keys` and that the file & parent directories have the correct permissions.

# Provisioning

```
ansible-playbook --ask-vault-pass tailscale.yml
```

## Client configuration

Follow [the guide](https://tailscale.com/kb/1039/install-ubuntu-2004) to install locally, but modify with the below command if you don't want the server to have remote access to your client.

```
sudo tailscale up --shields-up
```

## Next steps

I'd recommend reading [the ACL documentation](https://tailscale.com/kb/1018/acls). I've ended up with something like the below for ssh to my Tailscale server only:
```
{
  "Hosts": {
    "tailscale01": "100.100.100.1",
  },
  "ACLs": [
    {
      "Action": "accept",
      "Users": ["email@address.com"],
      "Ports": ["tailscale01:22"]
    },
  ]
}
```

Also, please make sure to keep your hosts patched & secure; run unattended-upgrades or update regularly, don't rely on Tailscale or Wireguard to save you from bad practices!
